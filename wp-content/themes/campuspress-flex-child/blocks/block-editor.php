<div id="itemsContainer">
    <a href="#left"><img id="left" class="left" src="<?php block_field( 'item1' ); ?>" alt="<?php block_field( 'author-name' ); ?>" /></a>
    <a href="#right"><img id="right" class="right" src="<?php block_field( 'item2' ); ?>" alt="<?php block_field( 'author-name' ); ?>" /></a>
</div>
<div id="contentContainerShell">
    <div id="contentContainer">
        <div id="left"><span id="title"><?php block_field( 'title' ); ?></span> <?php block_field( 'description' ); ?></div>
        <div id="right"><span id="title"><?php block_field( 'title1' ); ?></span> <?php block_field( 'description1' ); ?></div>
    </div>
</div>
<a href="<?php block_field( 'link' ); ?>" class="linkbutton">Read More</a>