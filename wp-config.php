<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'campuspress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'yGL_/-7O,$8).{A~~Z%.;3#`/-{W$Di},ntMf=x*_]zY`5YnPUZhH6Zp?@Qr;jqg' );
define( 'SECURE_AUTH_KEY',  '`AqF~_JQt+D3zR|e4;zMe3;m-{t]DvFeG jlJJ*8[C@a^>QTAy.B8h:4w?[zQ&)n' );
define( 'LOGGED_IN_KEY',    'ATxiG*UqTL5g?s<<y+a?p78(>(q<O~{`ee9#u)3&&u3,a,CYm<x5&dZ`e%uApt1r' );
define( 'NONCE_KEY',        '%A dmtbb#jM&1Pz^63Qqq{0w]?c#Gk28okV@*:3VcZ0=,7&y]`~q}~lmrC@e%<z ' );
define( 'AUTH_SALT',        '1vG)n:>V)2(1J!2ktTD?6rJnELSPjzGFJktw<<]?tO(}fjk.b@eU2^a*5ab2]y=V' );
define( 'SECURE_AUTH_SALT', 'G?37shrACEof|~!UAdY)FXa=)n$0Pk$GvB7E32-X!vZO/[alYMw9Fg-R!}o8a{*R' );
define( 'LOGGED_IN_SALT',   'k{IaLDwEJ]&6aS9+l&>$MeI14HH.g4Tbja&8,rM}{a:}tV|DgS,Oe;tM)=yDu$Z#' );
define( 'NONCE_SALT',       'J$tOd!/rL;,p[z/a[F;`<ex=%y2jTTY(!LVD|zx^jV)UkC%K*M{4wjrfsa3m~m45' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
